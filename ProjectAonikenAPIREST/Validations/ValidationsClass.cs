﻿using Newtonsoft.Json.Linq;
using ProjectAonikenAPI.Data;
using ProjectAonikenAPI.Helper;
using ProjectAonikenAPI.Models;

namespace ProjectAonikenAPI.Validations
{
    public class ValidationsClass
    {
        public static void validateDataOnGetEndpoint(JObject data) //Metodo unificado para facilitar implementacion de futuras validaciones para endpoint GET
        {
            EditorsDto? dataUser = data.ToObject<EditorsDto>();

            validateUser(dataUser);
        }
        public static void validateDataOnPutEndpoint(JObject data) //Metodo unificado para facilitar implementacion de futuras validaciones para endpoint PUT
        {
            BlogsDto? dataBlog = data.ToObject<BlogsDto>();
            EditorsDto? dataUser = data.ToObject<EditorsDto>();

            validateUser(dataUser);
            validateIdBlog(dataBlog.ID_BLOGS);
            validateStatusBlog(dataBlog.STATE);
            validatePendingBlog(dataBlog.ID_BLOGS);
        }

        public static void validateDataOnDeleteEndpoint(JObject data) //Metodo unificado para facilitar implementacion de futuras validaciones para endpoint PUT
        {
            BlogsDto? dataBlog = data.ToObject<BlogsDto>();
            EditorsDto? dataUser = data.ToObject<EditorsDto>();

            validateUser(dataUser);
            validateIdBlog(dataBlog.ID_BLOGS);
        }

        private static void validateDataNotNull(JObject? data)
        {
            if (data.ToObject<BlogsDto>() == null) 
            {
                throw new Exception("Faltan parametros del blog a modificar");
            }
            else if(data.ToObject<EditorsDto>() == null)
            {
                throw new Exception("Faltan ingresar un usuario editor");
            }
        }

        private static void validateUser(EditorsDto data)
        {
            var editorUser = APIHelper.getUserByUsernameandToken(data.USER, data.TOKEN);
            if (string.IsNullOrEmpty(editorUser.Result.USER) || string.IsNullOrEmpty(editorUser.Result.TOKEN))
            {
                throw new Exception("Usuario invalido");
            }
        }

        private static void validateStatusBlog(char state)
        {
            if (!((char)BlogStates.approve == state || (char)BlogStates.reject == state))
            {
                throw new Exception($"El STATE: '{state}' no corresponde a un valor valido");
            }
        }

        private static void validateIdBlog(int id)
        {
            if (id < 1)
            {
                throw new Exception("El ID es invalido");
            }
        }

        private static void validatePendingBlog(int id)
        {
            var pendingBlogList = APIHelper.getBlogsByState((char)BlogStates.pending).Result.Where(p => p.ID_BLOGS == id);
            if (!pendingBlogList.Any())
            {
                throw new Exception("El id del blog no corresponde a un blog pendiente");
            }
        }
    }
}
