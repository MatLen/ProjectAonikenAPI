﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using ProjectAonikenAPI.Models;
using ProjectAonikenAPI.Models.Functions;
using ProjectAonikenAPI.Validations;

namespace ProjectAonikenAPI.Controllers
{
    [ApiController]
    [Route("api/blogs")]
    public class BlogController : Controller //Controller base de la API de editores
    {
        private EditorFunctions function = new EditorFunctions();

        #region GetBlogs

        [HttpGet("GetAllPendingBlogs")]
        public async Task<ActionResult<List<BlogsDto>>> Get(JObject data)
        {            
            try
            {
                ValidationsClass.validateDataOnGetEndpoint(data);
                var response = await function.getBlogs();
                if(!(response.Count == 0))
                {
                    return Ok(response);
                }
                else
                {
                    return NotFound(response);
                }                
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        #endregion

        #region PutBlog

        [HttpPut("UpdateBlog")]
        public async Task<ActionResult<BlogsDto>> Put([FromBody]JObject data) //Por cuestiones de escalabilidad se decide pedir por parametros un JObject
        {
            try
            {
                ValidationsClass.validateDataOnPutEndpoint(data);

                BlogsDto dataBlog = data.ToObject<BlogsDto>();

                var response = await function.putBlogs(dataBlog);
                if (response.ID_BLOGS != -1)
                {
                    return Ok(response);
                }
                else
                {
                    return NotFound(response);
                }                
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        #endregion

        #region DeleteBlog

        [HttpDelete("DeleteBlog")]
        public async Task<ActionResult<string>> Delete([FromBody] JObject data) 
        {
            try
            {
                ValidationsClass.validateDataOnDeleteEndpoint(data);

                BlogsDto dataBlog = data.ToObject<BlogsDto>();

                var response = await function.deleteBlogs(dataBlog);

                if(response == 1)
                {
                    return Ok($"El blog de ID = {dataBlog.ID_BLOGS} fue eliminado");
                }
                else
                {
                    return NotFound($"No fue posible eliminar el blog de ID = {dataBlog.ID_BLOGS}");
                }
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        #endregion
    }
}
