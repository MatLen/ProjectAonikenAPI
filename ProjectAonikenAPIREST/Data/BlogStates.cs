﻿namespace ProjectAonikenAPI.Data
{
    public enum BlogStates //Valores posibles de STATE en request
    {
        approve = 'A',
        reject = 'R',
        pending = 'P'
    }
}
