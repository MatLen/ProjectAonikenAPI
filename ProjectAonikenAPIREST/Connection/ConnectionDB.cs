﻿namespace ProjectAonikenAPI.Connection
{
    public class ConnectionDB //debido a la potencial necesidad de requerir el connectionString, se crea esta clase
                              //para simplificar la obtencion del mismo
    {
        private string connectionString = string.Empty;
        public ConnectionDB()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            connectionString = builder.GetSection("ConnectionStrings:conexion").Value;
        }
        public string getSQLString()
        {
            return connectionString;
        }
    }
}
