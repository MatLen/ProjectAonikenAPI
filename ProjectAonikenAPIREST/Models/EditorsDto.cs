﻿using System.ComponentModel.DataAnnotations;

namespace ProjectAonikenAPI.Models
{
    public class EditorsDto //clase que mapea la tabla Editors de la base
    {
        [Key]
        public int ID_EDITOR { get; set; }
        public string USER { get; set; }
        public string PASSWORD { get; set; }
        public string? TOKEN { get; set; }
    }
}
