﻿using ProjectAonikenAPI.Connection;
using ProjectAonikenAPI.Helper;
using ProjectAonikenAPI.Data;

namespace ProjectAonikenAPI.Models.Functions
{
    public class EditorFunctions // Se encapsulan en una clase las funciones propias de los editores
    {
        ConnectionDB Connection = new ConnectionDB();
        public async Task<List<BlogsDto>> getBlogs()
        {
            return await APIHelper.getBlogsByState((char)BlogStates.pending);
        }
        public async Task<BlogsDto> putBlogs(BlogsDto blog)
        {
            return await APIHelper.updateBlogByIDandState(blog.ID_BLOGS, blog.STATE);
        }

        public async Task<int> deleteBlogs(BlogsDto blog)
        {
            return await APIHelper.deleteBlogByIDandState(blog.ID_BLOGS);
        }
    }
}
