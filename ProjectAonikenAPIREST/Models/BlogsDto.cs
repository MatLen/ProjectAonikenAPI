﻿namespace ProjectAonikenAPI.Models
{
    public class BlogsDto //clase que mapea la tabla Blogs de la base
    {
        public int ID_BLOGS { get; set; }
        public string TITLE { get; set; }
        public string? AUTHOR { get; set; }
        public DateTime Date { get; set; }
        public char STATE { get; set; }
    }
}
