﻿using ProjectAonikenAPI.Connection;
using ProjectAonikenAPI.Models;
using System.Data.SqlClient;

namespace ProjectAonikenAPI.Helper
{
    public class APIHelper //Se crea un helper para standarizar obtencion de registros al llamar a la base
    {
        public static async Task<EditorsDto> getUserByUsernameandToken(string username, string Token)
        {
            ConnectionDB Connection = new ConnectionDB();
            EditorsDto Editor = new EditorsDto();

            using (var sql = new SqlConnection(Connection.getSQLString()))
            {
                using (var cmd = new SqlCommand(QueryHelper.getQuerySelectUserByUsernameAndToken(username, Token), sql))
                {
                    try
                    {
                        await sql.OpenAsync();
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (var item = await cmd.ExecuteReaderAsync())
                        {
                            if (await item.ReadAsync())
                            {                                
                                Editor.USER = (string)item["USER"];
                                Editor.TOKEN = (string)item["Token"];
                                return Editor;
                            }
                            else
                            {
                                Editor.USER = "";
                                Editor.TOKEN = "";
                                return Editor;
                            }
                        }
                    }
                    catch (SqlException)
                    {
                        throw new Exception("Error al intentar conectar con la base de datos");
                    }
                }
            }
        }
        public static async Task<List<BlogsDto>> getBlogsByState(char state)
        {
            ConnectionDB Connection = new ConnectionDB();
            List<BlogsDto> lista = new List<BlogsDto>();

            using (var sql = new SqlConnection(Connection.getSQLString()))
            {
                using (var cmd = new SqlCommand(QueryHelper.getQuerySelectByState(state), sql))
                {
                    try
                    {
                        await sql.OpenAsync();
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (var item = await cmd.ExecuteReaderAsync())
                        {
                            while (await item.ReadAsync())
                            {
                                var blog = new BlogsDto();
                                blog.ID_BLOGS = (int)item["ID_BLOGS"];
                                blog.TITLE = (string)item["TITLE"];
                                blog.AUTHOR = (string)item["AUTHOR"];
                                blog.Date = (DateTime)item["DATE"];
                                blog.STATE = char.Parse(item["STATE"].ToString());
                                lista.Add(blog);
                            }
                        }
                    }
                    catch (SqlException)
                    {
                        throw new Exception("Error al intentar conectar con la base de datos");
                    }
                }
            }
            return lista;
        }

        public static async Task<BlogsDto> updateBlogByIDandState(int id, char state)
        {
            ConnectionDB Connection = new ConnectionDB();
            var blog = new BlogsDto();

            using (var sql = new SqlConnection(Connection.getSQLString()))
            {
                
                using (var cmd = new SqlCommand(QueryHelper.getQueryUpdateStateByID(id, state), sql))
                {
                    try
                    {                       
                        await sql.OpenAsync();
                        cmd.CommandType = System.Data.CommandType.Text;
                        await cmd.ExecuteNonQueryAsync();

                        cmd.CommandText = QueryHelper.getQuerySelectByIDandState(id, state);
                        var item = await cmd.ExecuteReaderAsync();

                        if (await item.ReadAsync())
                        {                            
                            blog.TITLE = (string)item["TITLE"];
                            blog.AUTHOR = (string)item["AUTHOR"];
                            blog.Date = (DateTime)item["DATE"];
                            blog.STATE = char.Parse(item["STATE"].ToString());
                        }
                        else
                        {
                            blog.ID_BLOGS = -1;
                            blog.STATE = '\0';
                        }
                        return blog;
                    }
                    catch (SqlException)
                    {
                        throw new Exception("Error al intentar conectar con la base de datos");
                    }
                }
            }
        }

        public static async Task<int> deleteBlogByIDandState(int id)
        {
            ConnectionDB Connection = new ConnectionDB();
            var blog = new BlogsDto();

            using (var sql = new SqlConnection(Connection.getSQLString()))
            {

                using (var cmd = new SqlCommand(QueryHelper.getQueryDeleteBlogById(id), sql))
                {
                    try
                    {
                        await sql.OpenAsync();
                        cmd.CommandType = System.Data.CommandType.Text;
                        var item = await cmd.ExecuteNonQueryAsync();

                        if (item != 1)
                        {
                            throw new Exception("No fue posible eliminar el blog");
                        }
                        else
                        {
                            return item;
                        }
                    }
                    catch (SqlException)
                    {
                        throw new Exception("Error al intentar conectar con la base de datos");
                    }
                }
            }
        }
    }
}
