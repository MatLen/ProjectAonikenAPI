﻿namespace ProjectAonikenAPI.Helper
{
    public static class QueryHelper
    {
        //Esta clase controla las query que se realizan sobre la base de datos para evitar consultas harcodeadas.

        //Eventualmente se podrïa validar el TypeOf de la clase que llame a la función para determinar si es correcto enviar o no la query solicitada.
        public static string getQuerySelectAllPendingBlogs()
        {
            return "select [ID_BLOGS],[TITLE],[AUTHOR],[DATE],[STATE] from Blogs WHERE STATE = 'P'";
        }
        public static string getQueryUpdateStateByID(int id, char state)
        {
            return $"UPDATE Blogs set STATE = '{state}' where ID_BLOGS = {id} AND STATE = 'P'";
        }
        public static string getQuerySelectByIDandState(int id, char state)
        {
            return $"select [ID_BLOGS],[TITLE],[AUTHOR],[DATE],[STATE] from Blogs WHERE STATE = '{state}' AND ID_BLOGS = {id}";
        }
        public static string getQuerySelectByState(char state)
        {
            return $"select [ID_BLOGS],[TITLE],[AUTHOR],[DATE],[STATE] from Blogs WHERE STATE = '{state}'";
        }

        public static string getQuerySelectUserByUsernameAndToken(string username, string Token)
        {
            return $"select [ID_EDITOR], [USER], [PASSWORD], [TOKEN] from Editors where [USER] = '{username}' AND [TOKEN] = '{Token}'";
        }
        public static string getQueryDeleteBlogById(int id)
        {
            return $"DELETE FROM BLOGS WHERE [ID_BLOGS] = '{id}'";
        }
    }
}
