# ProjectAonikenAPI

## Descripcion
API para manejar los post desde la optica de un usuario Editor.

## Aclaraciones previas
Es necesario contar con SQL Server para poder crear una base de datos que nos permita realizar el intercambio de informacion con la API en tiempo real.

## Instalacion
1) Correr Script: InitialScript_CreateTableBlogs_InsertSampleBlogs.sql
2) Desde nuestro IDE necesitamos modificar la propiedad conexion del archivo appsettings.json seteando el connectionString de la base que usaremos para almacenar la informacion.
3) Compilamos el proyecto.

## Ejecucion
1) Desde nuestro IDE ejecutamos la aplicacion.

## Pruebas 
#Aclaracion: Se recomienda utilizar postman para probar correctamente las llamadas a la API.

1) Los dos Endpoints posibles que brinda la API son:

GetAllPendingBlogs: Devuelve un JSON con un listado de los blogs en estado pendiente de la base.

UpdateBlog: Actualiza el estado de un blog especifico en base a un ID.
#Los datos de ID y STATE se envian a traves del body en forma de JSON desde Postman.



